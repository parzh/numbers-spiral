# numbers-spiral

HTML/CSS/TypeScript project that builds webpage with configurable spiral of numbers.

![](/docs/demo.png)

## Live Server

This project requires [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) extension to be installed in Visual Studio Code. It is already configured to serve files from `/public` folder to Chrome Incognito tab (the server is also watching changes).

To start server, run "Live Server: Open with Live Server" command (hotkey: <kbd>Alt</kbd> + <kbd>L</kbd> <kbd>Alt</kbd> + <kbd>O</kbd>):

![](/docs/live-server-open-command.png)

## Playing in browser

- install dependencies:
	```sh
	npm i
	```

- serve files from `/public`;

## Playing in editor

- install dependencies;
- serve files from `/public`;
- setup TypeScript watcher to listen changes in the code:
	```sh
	npm run dev
	```
- make changes in any `*.ts` file in `/src` folder;