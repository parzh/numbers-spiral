/** @private */
type PointArgs = [ /* distance */ number, /* angle */ number ];

/** @private */
type PointsCache = Partial<{
	[integer: number]: PointArgs;
}>;

/** @private */
type PointsCachesCollection = Partial<{
	[cacheId: string]: PointsCache;
}>;

// ***

/** @private */
const pointsCachesCollection: PointsCachesCollection = Object.create(null);

export function getCache(scale: number, constant: number): PointsCache {
	const cacheId = `${ scale }|${ constant }`;

	if (!(cacheId in pointsCachesCollection))
		pointsCachesCollection[cacheId] = {};

	return pointsCachesCollection[cacheId]!;
}
