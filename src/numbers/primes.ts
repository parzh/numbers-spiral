import { DEFAULT_MAX_VALUE } from "./constants";

/** @private */
const primes = [ 2, 3 ];

export default function * (maxValue: number = DEFAULT_MAX_VALUE): IterableIterator<number> {
	for (const value of primes)
		if (value <= maxValue)
			yield value;

		else return;

	const lastCached = primes[primes.length - 1];

	if (lastCached < maxValue)
		for (let value = lastCached; value <= maxValue; value += 2)
			if (primes.every((prime) => value % prime !== 0)) {
				primes.push(value);
				yield value;
			}
}
