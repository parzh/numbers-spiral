import { DEFAULT_MAX_VALUE } from "./constants";

export default function * (maxValue: number = DEFAULT_MAX_VALUE): IterableIterator<number> {
	for (let value = 1; value < maxValue; value += 2)
		yield value;
}
