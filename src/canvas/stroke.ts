import { getContext } from "./index";

export type StrokeOptions =
	& Take<CanvasFillStrokeStyles, "strokeStyle">
	& Take<CanvasRenderingContext2D, "lineWidth">;

/** @private */
const canvas = getContext();

export default function(options: StrokeOptions, ...paths: Path2D[]): void {
	canvas.lineWidth = options.lineWidth;
	canvas.strokeStyle = options.strokeStyle;

	for (const path of paths)
		canvas.stroke(path);
}
