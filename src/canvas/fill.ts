import { getContext } from "./index";

export type FillOptions = Take<CanvasRenderingContext2D, "fillStyle">;

/** @private */
const canvas = getContext();

export default function(options: FillOptions, ...paths: Path2D[]): void {
	canvas.fillStyle = options.fillStyle;

	for (const path of paths)
		canvas.fill(path);
}
