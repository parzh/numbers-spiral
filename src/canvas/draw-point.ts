import { WIDTH, HEIGHT, halfDimensions as half } from "./constants";
import fill from "./fill";

export interface DrawPointOptions {
	weight: number;
	insideCanvasOnly: boolean;
}

/** @private */
const draw = fill.bind(null, { fillStyle: "hsl(200, 100%, 80%)" });

export default async function(options: DrawPointOptions, distance: number, angle: number): Promise<void> {
	const x = half.width + Math.cos(angle) * distance;
	const y = half.height + Math.sin(angle) * distance;

	if (options.insideCanvasOnly)
		if (x < 0 || y < 0 || x > WIDTH || y > HEIGHT)
			return;

	const point = new Path2D();

	point.arc(x, y, options.weight / 2, Math.PI * 2, 0);

	draw(point);
}
