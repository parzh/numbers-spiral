import canvasElement from "../elements/canvas";
import { dimensions } from "./constants";

/** @private */
const canvas = canvasElement.attr(dimensions).css(dimensions);

export function getElement(): HTMLCanvasElement {
	return canvas[0];
}

/** @private @ignore */
let __isSupported: boolean | null = null;

/** @private */
function isSupported(): boolean {
	if (__isSupported === null)
		__isSupported = typeof getElement().getContext === "function";

	return __isSupported;
}

/** @private */
function assertSupport() {
	if (!isSupported())
		throw new Error("Canvas API is not supported");
}

/** @private */
type Context = CanvasRenderingContext2D;

/** @private */
let context: Context;

export function getContext(): Context {
	assertSupport();

	if (!context)
		context = getElement().getContext("2d", { alpha: true })!;

	return context;
}
