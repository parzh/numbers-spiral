import { WIDTH, HEIGHT, halfDimensions as half } from "./constants";
import stroke from "./stroke";

/** @private */
const HALF_MARK_WIDTH = 2;

/** @private */
const DISTANCE_BETWEEN_MARKS = 8;

// ***

/** @private */
function createAxes(): Path2D {
	const axis = new Path2D();

	// horizontal
	axis.moveTo(0, half.height);
	axis.lineTo(WIDTH, half.height);

	// vertical
	axis.moveTo(half.width, 0);
	axis.lineTo(half.width, HEIGHT);

	axis.closePath();

	return axis;
}

/** @private */
function createMarks(): Path2D {
	const marks = new Path2D();

	let position = 0;
	let hasDrawn = false;

	do {
		position += DISTANCE_BETWEEN_MARKS;
		hasDrawn = false;

		// horizontal
		if (position < half.width) {
			// right
			marks.moveTo(half.width + position, half.height - HALF_MARK_WIDTH);
			marks.lineTo(half.width + position, half.height + HALF_MARK_WIDTH);

			// left
			marks.moveTo(half.width - position, half.height - HALF_MARK_WIDTH);
			marks.lineTo(half.width - position, half.height + HALF_MARK_WIDTH);

			hasDrawn = true;
		}

		// vertical
		if (position < half.height) {
			// top
			marks.moveTo(half.width - HALF_MARK_WIDTH, half.height + position);
			marks.lineTo(half.width + HALF_MARK_WIDTH, half.height + position);
	
			// bottom
			marks.moveTo(half.width - HALF_MARK_WIDTH, half.height - position);
			marks.lineTo(half.width + HALF_MARK_WIDTH, half.height - position);

			hasDrawn = true;
		}
	} while (hasDrawn);

	marks.closePath();

	return marks;
}

// ***

/** @private */
const axes = createAxes();

/** @private */
const marks = createMarks();

// ***

/** @private */
const draw = stroke.bind(null, {
	lineWidth: 1,
	strokeStyle: "rgba(255, 255, 255, .1)",
});

export default async function(): Promise<void> {
	draw(axes, marks);
}
