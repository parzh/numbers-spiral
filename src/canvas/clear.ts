import { WIDTH, HEIGHT } from "./constants";
import { getContext } from "./index";

export default function(): void {
	getContext().clearRect(0, 0, WIDTH, HEIGHT);
}
