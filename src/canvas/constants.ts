export const WIDTH = Math.floor($("body").width()! / 2) * 2 - 1;
export const HALF_WIDTH = WIDTH / 2 + 1;

export const HEIGHT = Math.floor(($("html").height()! - 3) / 2) * 2 - 1;
export const HALF_HEIGHT = HEIGHT / 2 + 1;

export const DIAGONAL = Math.sqrt(WIDTH ** 2 + HEIGHT ** 2);
export const HALF_DIAGONAL = DIAGONAL / 2 + 1;

export const dimensions = Object.freeze(<const> {
	width: WIDTH,
	height: HEIGHT,
});

export const halfDimensions = Object.freeze(<const> {
	width: HALF_WIDTH,
	height: HALF_HEIGHT,
});
