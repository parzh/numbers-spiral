import limitInput from "./elements/limit.input";
import customConstantInput from "./elements/custom-constant.input";
import scaleInput from "./elements/scale.input";
import gridCheckboxInput from "./elements/grid-checkbox.input";
import numbersSelect from "./elements/numbers.select";
import constantSelect from "./elements/constant.select";

import { getCache } from "./points/cache";

import { HALF_DIAGONAL } from "./canvas/constants"
import drawGrid from "./canvas/draw-grid";
import drawPoint from "./canvas/draw-point";
import clear from "./canvas/clear";

import naturals from "./numbers/naturals";
import primes from "./numbers/primes";
import evens from "./numbers/evens";
import odds from "./numbers/odds";

/** @private */
const constants = <const> {
	E: Math.E,
	PI: Math.PI,
	TAU: Math.PI * 2,
	PHI: (1 + Math.sqrt(5)) / 2,
	SQRT2: Math.SQRT2,
};

/** @private */
type ConstantName = keyof typeof constants;

constantSelect.on("change", () => {
	const constantName = constantSelect.val() as string;

	if (constantName in constants)
		customConstantInput.val(constants[constantName as ConstantName]);
});

// ***

/** @private */
const numbers = <const> { primes, naturals, evens, odds };

/** @private */
type NumbersType = keyof typeof numbers;

// ***

/** @private */
const __drawPoint = drawPoint.bind(null, {
	weight: 1.3,
	insideCanvasOnly: true,
});

/** @private */
function main() {
	clear();

	if (gridCheckboxInput.prop("checked"))
		drawGrid();

	const scale = Number(scaleInput.val());

	if (!scale)
		return __drawPoint(0, 0);

	const constantName = <ConstantName> constantSelect.val();
	const constant = constants[constantName] || Number(customConstantInput.val());
	const cache = getCache(scale, constant);

	const numberType = <NumbersType> numbersSelect.val();
	const upperLimit = Number(limitInput.val());
	const sequence = numbers[numberType](upperLimit);

	for (const integer of sequence) {
		if (!(integer in cache))
			cache[integer] = [
				Math.sqrt(integer * scale),
				integer * constant * constants.TAU,
			];

		const [ distance, angle ] = cache[integer]!;

		if (distance < HALF_DIAGONAL)
			__drawPoint(distance, angle);

		else break;
	}
}

$(main);
$(".inputs select, .inputs input").on("change", main);
