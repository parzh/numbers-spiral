type Take<Obj extends object, Key extends keyof Obj> = {
	[_Key in Key]: Obj[_Key];
};

declare var canvas: HTMLCanvasElement;
